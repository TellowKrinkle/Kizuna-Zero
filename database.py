#JSON for future use
import json

#database class for fun module
class fun(object):

    #starting properties for "fun" database object
    def __init__(self, commands):
        for i in commands:
            setattr(self, i + "DB", dict())
        self.commands = commands
        '''
        It's the same as doing:
        self.hugDB = dict()
        self.touchDB = dict()
        '''

    #function to check if user exists in database
    def userExists(self, command, id):
        #user get attribute to grab right db based on properties defined
        data = getattr(self, command + "DB")
        #if user id is a key in that specific db return True
        if id in data:
            return True
        return False #else no results

    #function to add user to database
    def addEntry(self, command, id):
        #use previous function to check if user doesn't exists
        if not self.userExists(command, id):
            #user get attribute to grab right db based on properties defined
            data = getattr(self, command + "DB")
            #new entry for an id key will be another dictionary
            data[id] = dict()
            #dictionary within dictionary's key will be based on module command dictionary
            for x in self.commands[command]:
                data[id][x] = None
            return True
        #if user exists already return False
        return False

    #function to update counter for user
    def updateCounter(self, command, id):
        #if user exists
        if self.userExists(command, id):
            #grab database
            data = getattr(self, command + "DB")
            #check if counter data is "None"
            if data[id]["counter"]:
                #grab user based on id then their "counter" value and add 1
                data[id]["counter"] = data[id]["counter"] + 1
            else:
                data[id]["counter"] = 1
            #success return True
            return True
        #user does not exist in db return False
        return False

    #function to get user counter
    def getUserCount(self, command, id):
        #if user exists
        if self.userExists(command, id):
            #grab database
            data = getattr(self, command + "DB")
            if data[id]["counter"]:
                #success return amount
                return data[id]["counter"]
            #when the user has "None"
            else:
                return 0
        #user does not exist in db return False
        return False
