import asyncio
import config


async def leave(ctx, sys, kizuna):
    #Check if user is in admin list in config
    if ctx.message.author.id in config.admins:
        #leave message
        await kizuna.say("Peace out weebs.")
        #logout of session
        await kizuna.logout()
        #close websocket connections and whatnot
        await kizuna.close()
        #exit
        sys.exit()

async def e(ctx, kizuna, *args):
    if ctx.message.author.id in config.admins:
        #error handler
        try:
            proc = " ".join(args)
            ret = eval(proc)
            await kizuna.say(str(ret))
        except Exception as e:
            await kizuna.say(str(e))
